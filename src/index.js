import React from "react";
import ReactDOM from "react-dom";
import IframeResizer from "iframe-resizer-react";
import cookie from "react-cookies";
import axios from "axios";

//tracking the user on switching to another tab
document.addEventListener("visibilitychange", (event) => {
  if (document.visibilityState == "visible") {
    console.log("tab is active");
  } else {
    let payload = constructPayload("time");
    axios({
      method: "post",
      url: `https://syndication.weaveroo.io/media/${payload.d}/${payload.l}/${payload.p}`,
      params: payload
    })
      .then((data) => {
      })
      .catch((err) => {
      });
  }
});

//onload event trigger
document.onload = requireData();

//cart_click event trigger
let addToBasketBtn = document.getElementsByClassName("dc-icon-add-to-basket");
addToBasketBtn = addToBasketBtn[1].parentElement;
addToBasketBtn.addEventListener("click", clickEvent);

//form event to syndicate page
// var syndicateForm = document.getElementById("syndicate-form");

// syndicateForm.addEventListener("submit", generatePage);


function constructGenericData(attributes) {
  let obj = {};
  Object.entries(attributes).map((item) => {
    obj[item[0]] = item[1];
  });
  return obj;
}

function constructRetailerSpecificData(attributes){
  let obj = {};
  Object.entries(attributes).map((item) => {
    obj[item[0]] = item[1];
  });
  return obj;
}

function constructPayload(page_event){
  let data = document.getElementsByTagName("weaver");

  //Generic Attributes
  let et = page_event;
  const pn = window.location.href; //product page url

  let ti = document.getElementsByTagName("title"); //title of the page
  ti = ti[0].innerText;

  let h1 = document.getElementsByTagName("h1"); //h1 tag of the page
  h1 = h1[0].innerText;

  let img = document.querySelector('meta[property="og:image"]').content; //main product image

  let price = document.querySelector('meta[property="og:price:amount"]') //product price
    .content;

  let currency = document.querySelector('meta[property="og:price:currency"]') // currency
    .content;
    let ed = 0 
    if(page_event === "cart_click"){
        ed = 1;
    }
    else if(page_event === "time"){
      ed = "on_exit"
    }

  let genericAttributes = constructGenericData({
    et,
    pn,
    ti,
    h1,
    img,
    price,
    currency,
    ed
  });

    //Retailer Specific Attributes
    let d = data[0].getAttribute("d"); //dist ID
    let p = data[0].getAttribute("p"); //product code
    let mpn = data[0].getAttribute("mpn");
    let ean = data[0].getAttribute("ean");
    let sku = data[0].getAttribute("sku");
    let br = data[0].getAttribute("br"); //brand
    let l = data[0].getAttribute("l"); //language
  
  
    let retailerSpecificData = constructRetailerSpecificData({d,p,mpn,ean,sku,br,l});
    

  let payload = Object.assign(genericAttributes,retailerSpecificData);

  return payload;
}

export function requireData() {

  let payload = constructPayload("page");
  console.log(payload);
  

  axios({
    method: "post",
    url: `https://syndication.weaveroo.io/media/${payload.d}/${payload.l}/${payload.p}`,
    params: payload
  })
    .then((data) => {
    })
    .catch((err) => {
    });
  var appendParent = document.getElementById("iframe-form");
  if (appendParent.childNodes.length > 1) {
    appendParent.removeChild(appendParent.children[1]);
  }
  //can use this onload the page to syndicate the inpage

  let iframe = React.createElement(
    IframeResizer,
    {
      src: `https://syndication.weaveroo.io/media/${payload.d}/${payload.l}/${payload.p}`,
      id: "iframe1",
      frameBorder: 0,
      width: "100%",
      name: "myFrame",
      checkOrigin: false,
    },
    null
  );

  ReactDOM.render(iframe, appendParent);
}

function clickEvent() {
  let payload = constructPayload("cart_click");
  console.log(payload);
  
  

    axios({
      method: "post",
      url: `https://syndication.weaveroo.io/media/${payload.d}/${payload.l}/${payload.p}`,
      params: payload
    })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

// function generatePage(event) {
//   event.preventDefault();
//   let data = document.getElementsByTagName("weaver");
//   let retailerId = document.getElementById("retailerId").value;
//   let productCode = document.getElementById("productCode").value;

//   let iframe = React.createElement(
//     IframeResizer,
//     {
//       src: `https://syndication.weaveroo.io/media/${retailerId}/en/${productCode}`,
//       id: "iframe1",
//       frameBorder: 0,
//       width: "100%",
//       name: "myFrame",
//       autoResize: true,
//       scrolling: false,
//       checkOrigin: false,
//     },
//     null
//   );

//   ReactDOM.render(iframe, data[0]);
// }
